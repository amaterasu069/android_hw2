class BuildAircraft : Aircraft, Car {

    companion object Factory : AbstractFactory() {
        override fun getAircraft() = BuildAircraft()

        override fun getCar() = BuildAircraft()

    }

    override fun fly() {
        println("Aircraft flies")
    }

    override fun drive() {
        println("Car drives")
    }


}

fun main() {
    val factory: AbstractFactory = BuildAircraft.Factory
    val buildAircraft = factory.getAircraft()
    val buildCar = factory.getCar()

    buildAircraft.fly()
    buildCar.drive()
}
