import kotlin.math.pow
import kotlin.math.sqrt

class Point(private var x: Double, private var y: Double) {
    override fun toString(): String {
        return "($x, $y)"
    }

    fun equals(p: Point): Boolean {
        return x == p.x && y == p.y
    }

    fun distance(p: Point): Double {
        return sqrt((x - p.x).pow(2.0) + (y - p.y).pow(2.0))
    }

    fun move(x: Double, y: Double): Point {
        this.x += x
        this.y += y
        return this
    }

}

