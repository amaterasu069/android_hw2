fun main() {
    val firstPointInstance = Point(56.0,60.4)
    val secondPointInstance = Point(16.0,69.3)
    println(firstPointInstance.distance(secondPointInstance))
    println(firstPointInstance.equals(secondPointInstance))
    println(secondPointInstance.move(56.1,99.9).toString())

}